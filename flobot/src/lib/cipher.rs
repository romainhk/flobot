use flobot_lib::client;
use flobot_lib::handler::Handler as BotHandler;
use flobot_lib::models::Post;
use log::info;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fmt;

use crate::cipher_helpers::{
    compute_shift, get_simple_shift, normalize, orally_heard, orally_said, substitute,
    CountryCodes,
};

#[derive(Debug)]
pub enum Error {
    Client(String),
    Input(String),
    Other(String),
}

impl From<Error> for flobot_lib::handler::Error {
    fn from(e: Error) -> Self {
        match e {
            Error::Client(s) => flobot_lib::handler::Error::Timeout(s),
            Error::Input(s) => flobot_lib::handler::Error::Other(s),
            Error::Other(s) => flobot_lib::handler::Error::Other(s),
        }
    }
}

pub type Result = std::result::Result<String, Error>;

pub trait Cipher {
    fn cipher(&self, text: &str) -> Result;
    fn decipher(&self, text: &str) -> Result;
}

/// Init correct struct based on typed algorithm
trait FromAlgorithm<S> {
    fn from_algorithm(algorithm: &str) -> Option<S>;
}

/// Simple substitution vector
struct Substitution {
    key: Vec<i8>,
}

impl fmt::Display for Substitution {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Substitution()")
    }
}

impl FromAlgorithm<Substitution> for Substitution {
    fn from_algorithm(algorithm: &str) -> Option<Substitution> {
        let re = Regex::new(r"(shift|rot|vigenere)?\+?[a-zA-Z]*").unwrap();
        match re.captures(algorithm) {
            Some(capture) => match capture.get(1).map(|m| m.as_str()) {
                Some("") | Some("shift") | Some("rot") | None => {
                    if let Ok(shift) = get_simple_shift(algorithm) {
                        return Some(Substitution { key: vec![shift] });
                    }
                    return None;
                }
                Some("vigenere") => {
                    let key_vector: Vec<i8> = algorithm
                        .trim_start_matches("vigenere+")
                        .chars()
                        .filter(|c| c.is_alphanumeric())
                        .map(|c| compute_shift(c))
                        .collect();
                    return Some(Substitution { key: key_vector });
                }
                _ => return None,
            },
            None => return None,
        };
    }
}

impl Cipher for Substitution {
    fn cipher(&self, text: &str) -> Result {
        Ok(substitute(text, self.key.clone()))
    }

    fn decipher(&self, text: &str) -> Result {
        let key_vector: Vec<i8> = self.key.iter().map(|c| -c).collect();
        Ok(substitute(text, key_vector))
    }
}

/// All available algorithms
enum Algorithms {
    Radio(CountryCodes),
    Shift(Substitution),
    Vigenere(Substitution),
    Delastelle(Nomial),
}

impl fmt::Display for Algorithms {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Algorithms::Radio(_) => write!(f, "Radio()"),
            Algorithms::Shift(_) => write!(f, "Shift()"),
            Algorithms::Vigenere(_) => write!(f, "Vigenere()"),
            Algorithms::Delastelle(_) => write!(f, "Delastelle()"),
        }
    }
}

type PolybiusRectangle = HashMap<i8, Vec<char>>;
const SWITCH_KEY: char = '-';

/// Check and find a char in Polybius rectangle
trait ContainsChar {
    fn contains(&self, c: &char) -> bool;
    fn get_coordinates(&self, c: &char) -> String;
}

impl ContainsChar for PolybiusRectangle {
    fn contains(&self, c: &char) -> bool {
        for vec in self.values() {
            if vec.contains(&c) {
                return true;
            }
        }
        false
    }

    fn get_coordinates(&self, c: &char) -> String {
        if (*c == ' ' || *c == '.' || c.is_alphabetic()) && *c != SWITCH_KEY {
            let uppercase_c = c.to_uppercase().next().unwrap();
            for (key, val) in self.iter() {
                match val.iter().position(|&d| d == uppercase_c) {
                    Some(index) => {
                        if *key == -1 {
                            return index.to_string();
                        } else {
                            return key.to_string() + &index.to_string();
                        }
                    }
                    None => continue,
                }
            }
        }
        String::new()
    }
}

/// Delastelle's cipher parameters
struct Nomial {
    binome: String,
    square: PolybiusRectangle,
}

impl fmt::Display for Nomial {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Delastelle({})", self.binome)
    }
}

/// Specific trait for Delastelle based cipher
trait NomialTrait {
    fn new(monome: &str, binome: &str) -> Nomial;
    /// Generate the Polybius rectangle
    fn get_polybius_square(monome: &str, binome: HashSet<i8>) -> PolybiusRectangle;
}

impl Cipher for Nomial {
    fn cipher(&self, text: &str) -> Result {
        let mut result = String::new();
        for c in normalize(text).chars() {
            result.push_str(&self.square.get_coordinates(&c));
        }
        Ok(result)
    }

    fn decipher(&self, text: &str) -> Result {
        let mut result = String::new();
        let mut mode: i8 = -1;
        for c in text.chars() {
            let pivot: i8;
            if let Some(digit) = c.to_digit(10) {
                pivot = digit as i8;
            } else {
                continue;
            }
            if mode == -1 && self.binome.contains(c) {
                mode = pivot;
                continue;
            }
            if let Some(d) = self.square[&mode].get(pivot as usize) {
                result.push(*d);
            }
            mode = -1
        }
        Ok(result)
    }
}

impl FromAlgorithm<Nomial> for Nomial {
    fn from_algorithm(algorithm: &str) -> Option<Nomial> {
        let re = Regex::new(r"delastelle\+([a-zA-Z]+)(\d{2})").unwrap();
        match re.captures(algorithm) {
            Some(capture) => {
                return Some(Nomial::new(
                    capture.get(1).unwrap().as_str(),
                    capture.get(2).unwrap().as_str(),
                ))
            }
            None => return None,
        };
    }
}

impl NomialTrait for Nomial {
    fn new(monome: &str, binome: &str) -> Nomial {
        let binome_ = HashSet::from([
            binome.chars().nth(0).unwrap().to_digit(10).unwrap() as i8,
            binome.chars().nth(1).unwrap().to_digit(10).unwrap() as i8,
        ]);
        Nomial {
            binome: binome.to_string(),
            square: Nomial::get_polybius_square(monome, binome_),
        }
    }

    fn get_polybius_square(monome: &str, binome: HashSet<i8>) -> PolybiusRectangle {
        let mut rectangle = HashMap::new();
        let mut binome_: Vec<i8> = binome.iter().map(|&x| x as i8).collect();
        binome_.sort();
        let mut alphabet: [char; 28] = [' '; 28];
        for i in 0..26 {
            alphabet[i] = (b'A' + i as u8) as char;
        }
        alphabet[26] = '.';
        alphabet[27] = ' ';

        let mut line = vec![' '; 10];
        line[binome_[0] as usize] = SWITCH_KEY;
        line[binome_[1] as usize] = SWITCH_KEY;
        let mut index = 0;
        for c in monome
            .to_uppercase()
            .chars()
            .chain(alphabet.iter().cloned())
        {
            if line.contains(&c) || rectangle.contains(&c) {
                continue;
            }
            while line[index] == SWITCH_KEY {
                index += 1;
            }
            line[index] = c;
            index += 1;
            if index >= 10 {
                // Flush line
                for &i in &[-1, binome_[0], binome_[1]] {
                    if rectangle.contains_key(&i) {
                        continue;
                    }
                    rectangle.insert(i, line.clone());
                    line = vec![' '; 10]; // clear
                    index = 0;
                    break;
                }
            }
        }
        if !line.is_empty() {
            rectangle.insert(binome_[1], line.clone());
        }
        rectangle
    }
}

/// Init correct Algorithm based on detected cipher
fn create_algorithm(algorithm: &str) -> Option<Algorithms> {
    match algorithm {
        alg if alg.is_empty() || alg.starts_with("shift") || alg.starts_with("rot") => {
            if let Some(shift) = Substitution::from_algorithm(algorithm) {
                return Some(Algorithms::Shift(shift));
            }
        }
        alg if alg.starts_with("vigenere") => {
            if let Some(vector) = Substitution::from_algorithm(algorithm) {
                return Some(Algorithms::Vigenere(vector));
            }
        }
        alg if alg.starts_with("delastelle") => {
            if let Some(nomial) = Nomial::from_algorithm(algorithm) {
                return Some(Algorithms::Delastelle(nomial));
            }
        }
        alg if alg.starts_with("radio") => {
            let re = Regex::new(r"radio\+?([a-zA-Z]+)").unwrap();
            match re.captures(algorithm.to_lowercase().as_str()) {
                Some(capture) => match capture.get(1).map(|m| m.as_str()) {
                    Some("") | Some("fr") | None => {
                        return Some(Algorithms::Radio(CountryCodes::FR));
                    }
                    Some("be") => {
                        return Some(Algorithms::Radio(CountryCodes::BE));
                    }
                    Some("eo") => {
                        return Some(Algorithms::Radio(CountryCodes::EO));
                    }
                    Some("icao") => {
                        return Some(Algorithms::Radio(CountryCodes::ICAO));
                    }
                    Some("int") => {
                        return Some(Algorithms::Radio(CountryCodes::INT));
                    }
                    _ => return None,
                },
                None => return None,
            }
        }
        _ => return None,
    }
    None
}

impl Cipher for Algorithms {
    fn cipher(&self, text: &str) -> std::result::Result<String, Error> {
        match self {
            Algorithms::Radio(value) => {
                info!("Ciphering using Radio code");
                Ok(orally_said(value, text))
            }
            Algorithms::Shift(value) => {
                info!("Ciphering using Shift");
                Ok(value.cipher(text)?)
            }
            Algorithms::Vigenere(value) => {
                info!("Ciphering using Vigenere");
                Ok(value.cipher(text)?)
            }
            Algorithms::Delastelle(value) => {
                info!("Ciphering using Delastelle++");
                Ok(value.cipher(text)?)
            }
        }
    }

    fn decipher(&self, text: &str) -> std::result::Result<String, Error> {
        match self {
            Algorithms::Radio(value) => {
                info!("Deciphering using Radio code");
                Ok(orally_heard(value, text))
            }
            Algorithms::Shift(value) => {
                info!("Deciphering using Shift");
                Ok(value.decipher(text)?)
            }
            Algorithms::Vigenere(value) => {
                info!("Deciphering using Vigenere");
                Ok(value.decipher(text)?)
            }
            Algorithms::Delastelle(value) => {
                info!("Deciphering using Delastelle++");
                Ok(value.decipher(text)?)
            }
        }
    }
}

pub struct Handler<D> {
    client: D,
}

impl<D> Handler<D> {
    pub fn new(client: D) -> Self {
        Handler { client }
    }
}

impl<D> BotHandler for Handler<D>
where
    D: client::Sender + client::Editor,
{
    type Data = Post;

    fn name(&self) -> String {
        "cipher".into()
    }
    fn help(&self) -> Option<String> {
        Some(
            "```
!cipher text # cipher that with caesar algorithm, now!
!cipher @algorithm text # cipher using a specific algorithm
!decipher @algorithm text # decipher a text with the right algorithm
!cipher-algorithms # list available algorithms
Please note that all cipherings will lose all diacritical symbols but will leave most punctation symbols as is
```"
            .to_string(),
        )
    }

    fn handle(&self, post: &Post) -> flobot_lib::handler::Result {
        let input = &post.message;
        let algorithm: Option<&str>;
        let message: &str;

        if input.starts_with("!cipher-algorithms") {
            let message = "* @shift0 to 26 (equivalent with: @rot0 to 26): circular permutation of letters (aka 'caesar code')
* @vigenere+KEYWITHOUTSPACES: Vigenère cipher that do letter permutations according to a certain key (non-alphanumerical characters are ignored)
* @delastelle+KEYXX (XX are two numbers): improved Delastelle's cipher (called « monomial-binomial » cipher) based on a generated Polybius square
* @radio+LANGUAGE (be, fr, eo, icao, or int): spelling alphabet used for instance in radio communications";

            return Ok(self.client.post(&post.nmessage(&message))?);
        } else if input.starts_with("!cipher") {
            let re = Regex::new(r"^!cipher\s?@([\w\+]+)? (.*)$").unwrap();
            match re.captures(input) {
                Some(captures) => {
                    algorithm = captures.get(1).map(|m| m.as_str());
                    message = captures.get(2).unwrap().as_str();
                }
                None => {
                    let message = format!("Unmatched pattern: {}", input);
                    return Ok(self
                        .client
                        .post(&self.reply_error(&post, message).unwrap())?);
                }
            }

            match create_algorithm(algorithm.expect("Missing algorithm")) {
                Some(algorithm) => {
                    let processed_message = match algorithm.cipher(message) {
                        Ok(message) => message,
                        Err(error) => {
                            let message = format!("Can't cipher: {:?}", error);
                            return Ok(self
                                .client
                                .post(&self.reply_error(&post, message).unwrap())?);
                        }
                    };

                    self.client.reaction(post, "sleuth_or_spy")?;
                    return Ok(self.client.edit(&post, &processed_message)?);
                }
                None => {
                    return Err(flobot_lib::handler::Error::from(Error::Input(
                        format!("Unparsable algorithm {:?}", algorithm.unwrap())
                            .to_string(),
                    )));
                }
            }
        } else if input.starts_with("!decipher") {
            let re = Regex::new(r"^!decipher\s?@([\w\+]+)? (.*)$").unwrap();
            match re.captures(input) {
                Some(captures) => {
                    algorithm = captures.get(1).map(|m| m.as_str());
                    message = captures.get(2).unwrap().as_str();
                }
                None => {
                    let message =
                        format!("Unmatched pattern: {:?}", re.captures(input));
                    return Ok(self
                        .client
                        .post(&self.reply_error(&post, message).unwrap())?);
                }
            }

            match create_algorithm(algorithm.expect("Missing algorithm")) {
                Some(algorithm) => {
                    let processed_message = match algorithm.decipher(message) {
                        Ok(message) => message,
                        Err(error) => {
                            let message = format!("Can't decipher: {:?}", error);
                            return Ok(self
                                .client
                                .post(&self.reply_error(&post, message).unwrap())?);
                        }
                    };

                    self.client.reaction(post, "sleuth_or_spy")?;
                    return Ok(self.client.edit(&post, &processed_message)?);
                }
                None => {
                    return Err(flobot_lib::handler::Error::from(Error::Input(
                        format!("Unparsable algorithm {:?}", algorithm.unwrap())
                            .to_string(),
                    )));
                }
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod cipher {
    use super::*;

    #[test]
    fn test_polybius_square_contains() -> std::result::Result<(), Error> {
        let square: PolybiusRectangle = HashMap::from([
            (
                -1,
                vec![
                    'R', 'E', SWITCH_KEY, 'T', 'O', 'A', 'H', SWITCH_KEY, 'Y', 'B',
                ],
            ),
            (2, vec!['C', 'D', 'F', 'G', 'I', 'J', 'K', 'L', 'M', 'N']),
        ]);
        assert!(square.contains(&'A'));
        assert!(square.contains(&'R'));
        assert!(square.contains(&'J'));
        assert!(!square.contains(&'Z'));
        assert!(!square.contains(&' '));
        assert!(!square.contains(&'!'));
        Ok(())
    }

    #[test]
    fn test_polybius_square_get_coordinates() -> std::result::Result<(), Error> {
        let square: PolybiusRectangle = HashMap::from([
            (
                -1,
                vec![
                    'R', 'E', SWITCH_KEY, 'T', 'O', 'A', 'H', SWITCH_KEY, 'Y', 'B',
                ],
            ),
            (2, vec!['C', 'D', 'F', 'G', 'I', 'J', 'K', 'L', 'M', 'N']),
            (7, vec!['P', 'Q', 'S', 'U', 'V', 'W', 'X', 'Z', '.', ' ']),
        ]);
        assert_eq!(square.get_coordinates(&'R'), "0");
        assert_eq!(square.get_coordinates(&'r'), "0");
        assert_eq!(square.get_coordinates(&'Y'), "8");
        assert_eq!(square.get_coordinates(&'y'), "8");
        assert_eq!(square.get_coordinates(&'C'), "20");
        assert_eq!(square.get_coordinates(&'c'), "20");
        assert_eq!(square.get_coordinates(&'I'), "24");
        assert_eq!(square.get_coordinates(&'i'), "24");
        assert_eq!(square.get_coordinates(&'Z'), "77");
        assert_eq!(square.get_coordinates(&'z'), "77");
        assert_eq!(square.get_coordinates(&'1'), "");
        assert_eq!(square.get_coordinates(&'9'), "");
        assert_eq!(square.get_coordinates(&'.'), "78");
        assert_eq!(square.get_coordinates(&' '), "79");
        assert_eq!(square.get_coordinates(&'!'), "");
        assert_eq!(square.get_coordinates(&SWITCH_KEY), "");
        Ok(())
    }

    #[test]
    fn test_nomial_get_polybius_square() -> std::result::Result<(), Error> {
        let set: HashSet<i8> = HashSet::from([2 as i8, 7 as i8]);
        let result: PolybiusRectangle = HashMap::from([
            (
                -1,
                vec![
                    'R', 'E', SWITCH_KEY, 'T', 'O', 'A', 'H', SWITCH_KEY, 'Y', 'B',
                ],
            ),
            (2, vec!['C', 'D', 'F', 'G', 'I', 'J', 'K', 'L', 'M', 'N']),
            (7, vec!['P', 'Q', 'S', 'U', 'V', 'W', 'X', 'Z', '.', ' ']),
        ]);
        assert_eq!(
            Nomial::get_polybius_square("RETROAHOY", set.clone()),
            result
        );
        assert_eq!(Nomial::get_polybius_square("ReTroAhOY", set), result);

        let set: HashSet<i8> = HashSet::from([2 as i8, 1 as i8]);
        let result: PolybiusRectangle = HashMap::from([
            (-1, vec!['B', '-', '-', 'O', 'N', 'J', 'U', 'R', 'A', 'C']),
            (1, vec!['D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'P']),
            (2, vec!['Q', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z', '.', ' ']),
        ]);
        assert_eq!(Nomial::get_polybius_square("BONJOUR", set), result);
        Ok(())
    }

    #[test]
    fn test_nomial_cipher() -> std::result::Result<(), Error> {
        let nomial = Nomial::new("RETROAHOY", "27");
        assert_eq!(nomial.cipher("")?, "");
        assert_eq!(nomial.cipher("TRON")?, "30429");
        assert_eq!(nomial.cipher("É-TRON")?, "130429");
        assert_eq!(nomial.cipher("À'TRON-TÏon")?, "530429324429");
        assert_eq!(
            nomial.cipher("Modern software dev is, Mostly, overhead !")?,
            "284211029797242237550179211747924727928472327879474106152179"
        );
        Ok(())
    }

    #[test]
    fn test_nomial_decipher() -> std::result::Result<(), Error> {
        let nomial = Nomial::new("RETROAHOY", "27");
        assert_eq!(nomial.decipher("")?, "");
        assert_eq!(nomial.decipher("30429")?, "TRON");
        assert_eq!(nomial.decipher("130429")?, "ETRON");
        assert_eq!(nomial.decipher("530429324429")?, "ATRONTION");
        assert_eq!(
            nomial.decipher(
                "284211029797242237550179211747924727928472327879474106152179"
            )?,
            "MODERN SOFTWARE DEV IS MOSTLY OVERHEAD "
        );
        Ok(())
    }

    #[test]
    fn test_cipher_radio() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("radio+FR").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!(
            "HenriEugèneLouisLouisOscarDécimale WilliamOscarRaoulLouisDésiré ",
            &r1
        );
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO, WORLD ", &r2);

        algo = create_algorithm("radio+ICAO").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!(
            "HotelEchoLimaLimaOscarDecimal WhiskeyOscarRomeoLimaDelta ",
            &r1
        );
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO, WORLD ", &r2);
        Ok(())
    }

    #[test]
    fn test_cipher_rot() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("shift3").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Khoor, Zruog !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Khoor, Zruog !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("rot13").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Uryyb, Jbeyq !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);
        Ok(())
    }

    #[test]
    fn test_cipher_vigenere() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("vigenere+BONJOUR").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Isyuc, Qfszq !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+BON...").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Isymc, Jpfye !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+LES.SANGLOTS!LONGS-DES_VIOLONS.").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Siddo, Juczw !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+A").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Hello, World !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+D").unwrap();
        let r1 = algo.cipher("Héllo, World !")?; // aka Caesar
        assert_eq!("Khoor, Zruog !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);
        Ok(())
    }

    #[test]
    fn test_cipher_delastelle() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("delastelle+BONJOUR12").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("141117173292437171029", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO WORLD ", &r2);

        algo = create_algorithm("delastelle+BONJOUR21").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("141117173292437171029", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO WORLD ", &r2);

        let r1 = algo.cipher("Que j'aie une banque à chaque doigt ! Et un doigt dans chaque pays ! Que chaque pays soit à moi, Je sais quand même que chaque nuit...")?;
        assert_eq!("20611295815112964112908420611298299148206112910315132229291122296429103151322291084212991482061129198262129292061129914820611291982621292131522298291831529511292181521292068410291811181129206112991482061129461522282828", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("QUE JAIE UNE BANQUE A CHAQUE DOIGT  ET UN DOIGT DANS CHAQUE PAYS  QUE CHAQUE PAYS SOIT A MOI JE SAIS QUAND MEME QUE CHAQUE NUIT...", &r2);

        algo = create_algorithm("delastelle+SALUT26").unwrap();
        let r1 = algo.cipher("Bonjour! Tout le Monde")?;
        assert_eq!("72928252946269529456932069272928920", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("BONJOUR TOUT LE MONDE", &r2);
        Ok(())
    }
}
